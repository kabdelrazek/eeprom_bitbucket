#include <EEPROM.h>

//NOTE

//***FOR THIS TO WORK IN SERIAL MONTIOR GO TO TAB DIRECTLY LEFT OF "9600 BAUD"
//*** AND CHANGE IT TO "NEWLINE"

//ASSUMPTION MADE THAT NAMES  ARE NO MORE THAN 64 CHARACTERS

///////////////////////////////////////////


///////VARIABLES///////////

int tensDigit;
int onesDigit;

String name = " "; // Variable that concatenates characters of the SSID as they are entered in serial
String Wifi_string = " "; // Variable that concatenates characters of the Wifi_name is is entered in serial

boolean verificationTest = false;


boolean SSID_nameEntered = false;
boolean Wifi_nameEntered = false;

boolean SSID_confirmed = false;
boolean WifiPass_confirmed = false; 

boolean SSID_prompt_confirm = false;
boolean WifiPass_prompt_confirm = false;

boolean switchy = true;

byte value;

char SSID_name[64] ; //Assume SSID name will be no more than 64 characters
char Wifi_pass[64]; //Assume wifi_pass will be no more than 64 characters


//int address;
int  SSID_nameIndex = 0;
int  Wifi_passIndex = 0;

//////////// SETUP /////////////

void setup(){
  
  Serial.begin(9600);

//Experimental (CLEAR EEPROM) 
//for (int i = 0; i < 512; i++)
// EEPROM.write(i, 0);
//
 
//Initialize every slot in the array with special character '\0' to help detect null string
  for(int i = 0; i < 64; i++){

    SSID_name[i] = '\0';
    Wifi_pass[i] = '\0';
  }

//Serial.print("var a  is:"+ "\n");

//char convert = (char) a;

//Serial.print("A in ascii is:" + convert + "\n");
 
 //char lolchar = (char)ap;
 //Serial.print ("converted Char is" + lolchar);

}

//////////// LOOP /////////////////

void loop(){
 
 //-------------------------------------------- SSID PROMPT & SAVING TO MEMORY ---------------------------------------------------\\
 
 
  while(SSID_confirmed == false){
  
   
  if(SSID_prompt_confirm == false)
  {
  
  //Serial.print(char(ap));
  
  Serial.print("Please Input SSID: \n" );
  SSID_prompt_confirm = true;
  
  }
  
 //If new data has been entered, read each character into the name of the variable.
  while(Serial.available()){

    char readChar = (char)Serial.read();
    char readChar_clone = readChar;

    //If next character is a linefeed (enter) the name is complete
    if (readChar == '\n'){
      SSID_nameEntered = true;
      continue;
    }

    //If terminating character (\n) wasn't entered, add it to the name string and SSID_name.
     name += readChar;
     SSID_name[SSID_nameIndex]=readChar_clone;
     SSID_nameIndex++;
     
    }
    
    //If name has been completely entered ("followed by \n")
    //  1 - Print "My name is " + name
    //  2 - Write name into EEPROM one byte at a time, reading each byte from SSID_name array
     if(SSID_nameEntered){
      Serial.print ("------------------------------------------------------------------------\n");
      Serial.print ("The name of the SSID is now :" + name + "\n" );
      
      for (int i = 0; i < 64; i++){

        EEPROM.write(i, SSID_name[i]);
       
        // Check for terminating character '\0'. If found, break
        if(SSID_name[i] == '\0'){
              
              int SSID_intlen = i;
              String SSID_strlen = String(i);
              
              //Print length of name for verification
              Serial.print("SSID length is: " + SSID_strlen + "\n");
              
              //If the length of the SSID is greater than 10, we need 3 bytes. 2 to store the number as separate digits and 1 to verify whether the number is two digits long
              if(SSID_intlen >=10){
                
               // Serial.print ("So it is two digits! \n");
                
                tensDigit =   (( (SSID_intlen)/10 ) % 10) ;
                onesDigit =   ( (SSID_intlen) % 10 );
                
                Serial.print("Writing SSID to EEPROM... \n");
               // Save split the tens and ones digit of the SSID and write it to the EEPROM so you can combine it when reading the value back
                EEPROM.write(64,char(tensDigit));
                EEPROM.write(65,char(onesDigit));
                EEPROM.write(66, 'T'); // 'T' stands for TRUE is used to verify to the reading function that the SSID MUST BE two digits long.
               // Serial.print("The tensDigit is" + String(tensDigit)+  " and the ones Digit is " + String(onesDigit) + "\n" );
                 
              }
                       
              else{
                
              Serial.print("Writing SSID to EEPROM... \n"); 
              EEPROM.write(64,SSID_intlen);
              EEPROM.write(66,'F'); // Write a value other than 'T' to byte 66 to let EEPROM reading function known SSID MUST BE one digit long.
              
              }
           
              Serial.print("Null character detected! SSID succesfully stored in EEPROM! \n");
             Serial.print ("------------------------------------------------------------------------\n");
              break;
              
         }
       
       
      }
      
       SSID_nameEntered = false;
       SSID_confirmed = true;  
        
     // Serial.print("SSID WRITTEN, MOVING ON \n");    
     }
     
  }// END  OF SSID confirmation
  
  
  //------------------------------------------------ Wi-Fi PROMPT & SAVING TO MEMORY------------------------------------------------\\
  
  while(WifiPass_confirmed == false )
  {
    
    //Prompt User For WiFi password
    if(WifiPass_prompt_confirm == false)
    {
      Serial.print("Please type in Wifi password:  \n");
      WifiPass_prompt_confirm = true;
    }
    
  while(Serial.available()){

    char readChar = (char)Serial.read();
    char readChar_clone = readChar;

    //If next character is a linefeed (enter) the name is complete
    if (readChar == '\n'){
      Wifi_nameEntered = true;
      continue;
    }

    //If terminating character (\n) wasn't entered, add it to the name string and Wifi_name.
     Wifi_string += readChar;
     Wifi_pass[Wifi_passIndex]=readChar_clone;
     Wifi_passIndex++;
     
    }
    
    //If Wifi name has been completely entered ("followed by \n")
    //  1 - Print "Wifi " + name
    //  2 - Write name into EEPROM one byte at a time, reading each byte from SSID_name array
     if(Wifi_nameEntered){
       
      Serial.print ("Wi-Fi password is now: " + Wifi_string + "\n" );
      
      int WifiIndex = 0; // Create new index variable so that ith position in Wifi_pass corresponds to 64-128th position in EEPROM byte array
      
      for (int i = 67; i < 130; i++){

        EEPROM.write(i, Wifi_pass[WifiIndex]);
        WifiIndex++;
       
      
        // Check for terminating character '\0'. If found, break
        if(Wifi_pass[WifiIndex] == '\0'){
          
              Serial.print("Null character detected! Wi-Fi Password successfully stored in EEPROM!\n");
              
              //Print length of password for verification
             int Wifi_intlen = WifiIndex;
             Serial.print("The length of the Wi-Fi password is: " +  String(Wifi_intlen) + "\n");
             
              //If the length of the SSID is greater than 10, we need 3 bytes. 2 to store the number as separate digits and 1 to verify whether the number is two digits long
              if(Wifi_intlen >=10){
                
                Serial.print (" So it is two digits! ");
                
                tensDigit =   (( (Wifi_intlen)/10 ) % 10) ;
                onesDigit =   ((Wifi_intlen) % 10 );
                
                Serial.print("Writing SSID to EEPROM... \n");
               // Save split the tens and ones digit of the SSID and write it to the EEPROM so you can combine it when reading the value back
                EEPROM.write(130,char(tensDigit));
                EEPROM.write(131,char(onesDigit));
                EEPROM.write(132, 'T'); // 'T' stands for TRUE is used to verify to the reading function that the SSID MUST BE two digits long.
               // Serial.print("The tensDigit is" + String(tensDigit)+  " and the ones Digit is " + String(onesDigit) + "\n" );
                 
              }
                       
              else{
                
              Serial.print("Writing Wi-Fi Password to EEPROM... \n"); 
              EEPROM.write(130,Wifi_intlen);
              EEPROM.write(132,'F'); // Write a value other than 'T' to byte 66 to let EEPROM reading function known SSID MUST BE one digit long.
              
              }
             Serial.print ("------------------------------------------------------------------------\n");
             break;
              
         }
       
       
      }
      
       Wifi_nameEntered = false;
       WifiPass_confirmed = true;  
     // Serial.print("Wi-fi Password WRITTEN, MOVING ON \n");    
     }
    
   
    
  } // END Of Wifi Reading/Writing
  
 
 //TEST
 if(verificationTest == false)
 {
   EEPROM_readSSID();
   EEPROM_read_WifiPass();
   verificationTest= true;
 }
 
  
   
 }//End Brace For LOOP()


 //------------------------------------------------ FUNCTIONS TO READ FROM EEPROM ------------------------------------------------\\
 
//READ SSID FROM EEPROM
void EEPROM_readSSID()
{
  
  String SSID_string = "";
  String SSID_strlen = "";
  int SSID_length;
  char SSID_charbuf[3]; 
  byte value;
 
  Serial.print("Reading back SSID from EEPROM... \n" ) ;
  
  // Read byte 66 of EEPROM first to see if the number is two-digits long
  if( char(EEPROM.read(66)) == 'T'){
    
    // If SSID is two digits long do the following:
    // 1) Represent it as a string
    // 2) Break string into character array that has a NULL character at the end. (Accomplished by insiating SSID_charbuf[3]. Third spot is NULL character by default.
    // 3)  Call built-in  atoi()  function to convert to integer
    
    SSID_strlen = String(EEPROM.read(64)) + String(EEPROM.read(65));
    SSID_strlen.toCharArray(SSID_charbuf,3);
    SSID_length = atoi(SSID_charbuf);
    
    //Serial.print ("SSID length should be:  " + String(SSID_length)   + "\n");
    
  }
  
  else{
    
  SSID_length = int(char(EEPROM.read(64)));
  Serial.print ("SSID length should be: " + String(SSID_length)   + "\n");
  }
 
  for( int i = 0; i < SSID_length ; i++){
    
  value = EEPROM.read(i);
  char charval = char(value);
  SSID_string += charval;
  
 //Serial.print ("The value at index " + String(i) + " is "+ String(charval) + " and the value of SSID is currently " + SSID_string + " \n"  );
  //delay(10);
  }

Serial.print("The SSID in EERPROM is: " + SSID_string + "\n"); 
Serial.print("Reading Complete! \n");
Serial.print ("------------------------------------------------------------------------\n");
}

 //READ WI-FI PASSWROD FROM EEPROM
 
void EEPROM_read_WifiPass(){
  
  String Wifi_string = "";
  String Wifi_strlen = "";
  int Wifi_length;
  char Wifi_charbuf[3]; 
  byte value;
 
  Serial.print("Reading back Wi-Fi Password from EEPROM... \n" ) ;
  
  // Read byte 132 of EEPROM first to see if the Wi-Fi password is two-digits long
  if( char(EEPROM.read(132)) == 'T'){
    
    // If SSID is two digits long do the following:
    // 1) Represent it as a string
    // 2) Break string into character array that has a NULL character at the end. (Accomplished by insiating SSID_charbuf[3]. Third spot is NULL character by default.
    // 3)  Call built-in  atoi()  function to convert to integer
    
    Wifi_strlen = String(EEPROM.read(130)) + String(EEPROM.read(131));
    Wifi_strlen.toCharArray(Wifi_charbuf,3);
    Wifi_length = atoi(Wifi_charbuf);
    
    Serial.print ("Wifi length should be: " + String(Wifi_length)   + "\n");
    
  }
  
  else{
    
  Wifi_length = int(char(EEPROM.read(130)));
  Serial.print ("Wifi Length is:  " + String(Wifi_length)   + "\n");
  }
 
 
  for( int i = 0; i < Wifi_length ; i++){
    
  value = EEPROM.read(i+67);
  char charval = char(value);
  Wifi_string += charval;
  
 //Serial.print ("The value at index " + String(i) + " is "+ String(charval) + " and the value of SSID is currently " + SSID_string + " \n"  );
 //delay(10);
  }

Serial.print("The Wi-Fi Password is: " + Wifi_string + "\n"); 
Serial.print("Reading Complete! \n");
Serial.print ("------------------------------------------------------------------------\n");
  
}
